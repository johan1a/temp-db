# temp-db

A Clojure library designed to ... well, that part is up to you.

## Usage

FIXME

## Database setup

```
docker run --rm -d -p 5432:5432 --name postgres postgres


. .env
./scripts/create_local_db.sh
```

## Running

```
lein run
```

## Querying the API

```
export id=$(uuidgen)
curl -X POST -d '{"slapp T-shirt": true}' "https://traeskfindr.com/data/$id"
curl "https://traeskfindr.com/data/$id"
# ????
```

## License

Copyright © 2021 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
