(ns temp-db.core
  (:require [ring.adapter.jetty :as jetty]
            [ring.util.request :refer :all]
            [clj-postgresql.core :as pg]
            [clojure.java.jdbc :as jdbc])
  (:gen-class))

(def pooled-db (pg/pool :password (System/getenv "PGPASSWORD")))

(defn get-from-db
  [path]
  (jdbc/query pooled-db ["select data, content_type from records where path = ?" path]))

(defn save-to-db
  [path content-type data]
  (let [existing (get-from-db path)]
    (println existing)
    (if (empty? existing)
        (jdbc/insert! pooled-db :records {:path path :content_type content-type :data data})
        (jdbc/update! pooled-db :records {:content_type content-type :data data} ["path = ?" path]))))

(defn get-body
  [request]
  (ring.util.request/body-string request))

(defn get-content-type
  [request]
  (:content-type request))

(defn normalize-trailing-slash
  [request]
  (let [path (:uri request)]
    (if (clojure.string/ends-with? path "/")
      request
      (assoc request :uri (str path "/")))))

(defn not-found
  []
  {:status 404
   :headers {"Content-Type" "text/plain"}
   :body "What you seek is lost in the traesk..."})

(defn handle-get
  [request]
  (let [path (:uri request)
        record (first (get-from-db path))]
    (if (= record nil)
      (not-found)
      {:status 200
       :headers {"Content-Type" (or (:content_type record) "text/plain")}
       :body (:data record)})))

(defn handle-upsert
  [request]
  (let [path (:uri request)
        content-type (get-content-type request)
        data (:body request)
        result (save-to-db path content-type data)]
    {:status 201
     :headers {"Content-Type" content-type}
     :body data}))

(defn handler [request]
  (let [request-method (:request-method request)]
    (println (str "got request" request))
    (try
      (case request-method
        :get (handle-get request)
        :post (handle-upsert request)
        :put (handle-upsert request))
      (catch Exception e
        (println (str "Got exception: " e))
                 (not-found)))))

(defn entrypoint
  [request]
  (let [with-body (assoc request :body (get-body request))
        with-trailing-slash (normalize-trailing-slash with-body)]
    (handler with-trailing-slash)))

(defn -main
  [& args]
  (jetty/run-jetty entrypoint {:port  3000}))
