FROM java:8-alpine

WORKDIR /app

RUN mkdir /app/log

EXPOSE 3000

COPY target/*-standalone.jar /app/server.jar

CMD ["java", "-jar", "/app/server.jar"]
