#!/bin/sh
docker exec postgres psql -U postgres -d postgres -c "
create user $PGUSER with password '$PGPASSWORD';
"

docker exec postgres psql -U postgres -d postgres -c "
create database $PGDATABASE owner $PGUSER;
"

docker exec postgres psql -U postgres -d $PGDATABASE  -c "
create extension if not exists \"uuid-ossp\";

create table if not exists records (
   id uuid primary key default uuid_generate_v4(),
   path text unique not null,
   content_type text,
   data text not null
);

create or replace function trigger_set_timestamp()
returns trigger as $$
begin
  new.updated_at = now();
  return new;
  end;
$$ language plpgsql;

alter table records add column created_at timestamptz not null default now();
alter table records add column updated_at timestamptz not null default now();

create trigger set_timestamp
before update on records
for each row
execute procedure trigger_set_timestamp();

create unique index path_idx on records(path);

alter table public.records owner to $PGUSER;
"
