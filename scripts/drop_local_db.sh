#!/bin/sh
docker exec postgres psql -U postgres -d postgres -c "drop database if exists $PGDATABASE"

docker exec postgres psql -U postgres -d postgres -c "
drop role if exists $PGUSER;

drop index if exists path_idx;
"
