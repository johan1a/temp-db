#!/bin/sh -e
TAG=$(git rev-parse HEAD)
docker build -t johan1a/temp-db:${TAG} .
docker build -t johan1a/temp-db:latest .
