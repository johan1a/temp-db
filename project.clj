(defproject temp-db "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring/ring-core "1.8.2"]
                 [ring/ring-jetty-adapter "1.8.2"]
                 [clj-postgresql "0.7.0"]]
  :main temp-db.core
  :aot [temp-db.core]
  :plugins [[lein-cljfmt "0.7.0"]])
